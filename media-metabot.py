#!/usr/bin/env python
# Media MetaBot v1 -- a DaVinci Resolve Plug-In Script
# Copyright (C) 2024 Unresolved <unresolved-issues@protonmail.com>
# License for this Python script:  GNU General Public License, version 3
#                                  https://www.gnu.org/licenses/gpl-3.0.txt
# See README.md for instructions, terms, and more.  Use at your own risk.

import base64
import http.client
import json
import mimetypes
import os
import re
import ssl
import time
import urllib
import unicodedata

import DaVinciResolveScript as dvr_script

settings = {
    "url": '',  # example:  https://api.openai.com:443 or https://localhost:9443
    "api_key": '',
    "describe_prompt": "Describe this image.",
    "keyword_prompt": "Give five keywords (or tags) separated by commas (,) to help identify this image.",
    "do_description": True,
    "do_keywords": True,
    "do_disable_SSL_verification": False,
    "vision_model": "gpt-4-turbo",  # or "gpt-4-vision-preview", "llava", etc.
    "max_tokens": 300,
    "allow_http": False,
}

# constants
SCRIPT_TITLE = "Media MetaBot for DaVinci Resolve"
SETTINGS_FILE = os.path.join(os.path.expanduser("~"), "media-metabot-settings.json")
INSTRUCTION_TEXT = "Multimodal AI to auto-generate media pool metadata"
NOTE_TEXT = (
    "Important: THE MEDIA WILL BE SENT TO THE END POINT SERVER FOR PROCESSING. This script replaces previous"
    " metadata. Use this w/OpenAI's service or an OpenAI API-compatible, locally-hosted server offering LlaVA,"
    " Vision Transformer, Vision Mamba, etc. It is your responsibility to consider and respect relevant privacy"
    " policies and to fully comply with all relevant licenses as described by AI service provider(s), model"
    " cards, etc. You agree to use this script entirely at your own risk and in compliance with all applicable"
    " licenses and terms."
)
BAD_CSS = f"""color: rgb(205, 110, 110);"""
GOOD_CSS = f"""color: rgb(255, 255, 255);"""
FANCY_CSS = f"""
color: rgb(180, 180, 200);
font-family: Palatino;
font-size:18px;
"""
# regex thanks to https://stackoverflow.com/questions/106179/regular-expression-to-match-dns-hostname-or-ip-address
HOSTNAME_REGEX = r"^(?=.{1,253}\.?$)(?:(?!-)[-a-zA-Z0-9]{1,63}(?<!-)\.)*(?!-)[-a-zA-Z0-9]{1,63}(?<!-)\.?$"
IP_REGEX = \
    r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"

# initialize globals
resolve = dvr_script.scriptapp("Resolve")
project_manager = resolve.GetProjectManager()
current_project = project_manager.GetCurrentProject()
media_pool = current_project.GetMediaPool()
folder = media_pool.GetCurrentFolder()
original_ssl_default_context = ssl._create_default_https_context  # store in case we need it later. Hacky.

# create main UI Window
fusion = resolve.Fusion()
ui = fusion.UIManager
disp = dvr_script.UIDispatcher(ui)
win = disp.AddWindow({'WindowTitle': SCRIPT_TITLE, 'ID': 'TagWin', 'MinimumSize': [1200, 900], })
items = None  # this is where the items will go.


def save_settings(ui):
    json_settings = json.dumps(settings)
    with open(SETTINGS_FILE, "w") as f:
        f.write(json_settings)


def update_settings(ui):
    global settings
    settings["url"] = items['UrlLineEdit'].Text
    settings["api_key"] = items['APIKeyLineEdit'].Text
    settings["vision_model"] = items['ModelLineEdit'].Text
    settings["max_tokens"] = items['MaxTokenSpinBox'].Value
    settings["describe_prompt"] = items['DescribePromptText'].Text
    settings["keyword_prompt"] = items['KeywordPromptText'].Text
    settings["do_keywords"] = items['cbKeywords'].Checked
    settings["do_description"] = items['cbDescription'].Checked
    settings["allow_http"] = items['cbAllowHttp'].Checked
    settings["do_disable_SSL_verification"] = items['cbDisableSSLVerification'].Checked
    try:
        parsed = urllib.parse.urlparse(items['UrlLineEdit'].Text)
        if ((parsed.scheme == "https" or (settings["allow_http"] is True and parsed.scheme == "http"))
                and parsed.hostname is not None and parsed.port is not None
                and parsed.path == ''
                and (re.match(HOSTNAME_REGEX, parsed.hostname) or re.match(IP_REGEX, parsed.hostname))):
            items['UrlLineEdit'].SetStyleSheet(GOOD_CSS)
            win.Find("ButtonStart").Enabled = True
            win.Find("TimelineButton").Enabled = True
        else:
            items['UrlLineEdit'].SetStyleSheet(BAD_CSS)
            win.Find("ButtonStart").Enabled = False
            win.Find("TimelineButton").Enabled = False
    except ValueError:
        items['UrlLineEdit'].SetStyleSheet(BAD_CSS)
        win.Find("ButtonStart").Enabled = False
        win.Find("TimelineButton").Enabled = False


def load_settings():
    global settings
    if os.path.isfile(SETTINGS_FILE):
        try:
            with open(SETTINGS_FILE, "r") as f:
                json_settings = f.read()
            settings = json.loads(json_settings)
        except Exception as e:
            print(f"Problem with settings file:\n{str(e)}.\n\nUsing default settings.")


def get_llm_text_image(image_path, prompt, image_type):
    try:
        with open(image_path, "rb") as image_file:
            base64_image = base64.b64encode(image_file.read()).decode('utf-8')
        return True, f"{api_query(base64_image, prompt, image_type.lower())}"
    except Exception as e:
        show_error(str(e))
        return False, f"Error: {str(e)}"


def get_llm_text_thumbnail(thumbnail, prompt, image_type='x-ppm'):
    try:
        # make it an actual .ppm image, not just raw RGB
        ppm_header = "P6\n{} {}\n255\n".format(thumbnail["width"], thumbnail["height"]).encode()
        decoded_data = base64.b64decode(thumbnail["data"])
        base64_image = base64.b64encode(ppm_header + decoded_data).decode('utf-8')
        return True, f"{api_query(base64_image, prompt, image_type.lower())}"
    except Exception as e:
        show_error(str(e))
        return False, f"Error: {str(e)}"


def collect_clip_and_thumbnail():
    global current_project
    current_project = project_manager.GetCurrentProject()  # refresh
    timeline = current_project.GetCurrentTimeline()
    if timeline is None:
        show_error("There is no current timeline. Try again.")
        return False, False
    else:
        myPage = resolve.GetCurrentPage()
        if myPage != "color":
            show_error("Clip-on-timeline selection works best in the Color page. Try again.")
            resolve.OpenPage("color")
            return False, False
        else:
            thumbnail = timeline.GetCurrentClipThumbnailImage()
            resolve.OpenPage(myPage)
            if thumbnail is None or (len(thumbnail) == 0):
                show_error("No thumbnail image found."
                           " Ensure \"Clips\" is enabled, a thumbnail is selected, and try again.")
                return False, False
            else:
                item = timeline.GetCurrentVideoItem()
                if item is None:
                    show_error("A clip needs to be selected in the timeline. Try again.")
                    return False, False
                else:
                    clip = item.GetMediaPoolItem()
                    if clip is None:
                        show_error(f"The clip \"{item.GetName()}\" can't be found in the media pool. Try again.")
                        return False, False
                    else:
                        return clip, thumbnail


# get the stills in current bin.
def collect_stills():
    global folder
    folder = media_pool.GetCurrentFolder()
    clips = folder.GetClipList()
    stills = []
    for clip in clips:
        prop = clip.GetClipProperty()
        clipType = mimetypes.guess_type(prop['File Path'])[0]
        if prop["Type"] == "Still" or (clipType is not None and clipType.startswith("image/")):
            stills.append(clip)
    return stills


def process_clips(clips, desc_text, keyword_text, do_thumbnail=False):
    win.Find('MetadataTree').Clear()
    win.Find("ButtonStart").Enabled = False
    win.Find("TimelineButton").Enabled = False
    thumbnail = None
    success = False
    if settings["do_disable_SSL_verification"]:
        # security hack to not verify certificate -- not recommended
        ssl._create_default_https_context = ssl._create_unverified_context
    else:
        ssl._create_default_https_context = original_ssl_default_context
    if do_thumbnail:
        clip, thumbnail = collect_clip_and_thumbnail()
        if not thumbnail:
            win.Find("ButtonStart").Enabled = True
            win.Find("TimelineButton").Enabled = True
            return
        clips = [clip]
    for index, clip in enumerate(clips):
        prop = clip.GetClipProperty()
        items['status'].Text = f"Processing {index + 1} of {len(clips)}:  {clip.GetName()} ({prop['File Path']})..."
        if settings["do_description"]:
            if not do_thumbnail:
                success, description = get_llm_text_image(str(prop["File Path"]), desc_text, prop['Format'])
            else:
                success, description = get_llm_text_thumbnail(thumbnail, desc_text)
            itm = win.Find('MetadataTree').NewItem()
            itm.WordWrap = True
            itm.Text[0] = clip.GetName()
            itm.Text[1] = "Description"
            itm.Text[2] = description
            itm.Hidden = False
            if success:
                win.Find('MetadataTree').AddTopLevelItem(itm)
                win.Find('MetadataTree').ScrollToItem(itm)
                clip.SetClipProperty("Description", description)
            else:
                break
        if settings["do_keywords"]:
            if not do_thumbnail:
                success, keywords = get_llm_text_image(str(prop["File Path"]), keyword_text, prop['Format'])
            else:
                success, keywords = get_llm_text_thumbnail(thumbnail, keyword_text)
            itm = win.Find('MetadataTree').NewItem()
            itm.WordWrap = True
            itm.Text[0] = clip.GetName()
            itm.Text[1] = "Keywords"
            itm.Text[2] = keywords
            itm.Hidden = False
            win.Find('MetadataTree').AddTopLevelItem(itm)
            win.Find('MetadataTree').ScrollToItem(itm)
            if success:
                meta = clip.GetMetadata()
                meta["Keywords"] = keywords
                clip.SetMetadata(meta)
            else:
                break
    if success:
        items['status'].Text = "Done! Go check your clip metadata."
    else:
        items['status'].Text = "Unsuccessful. Investigate, fix, then try again."
    win.Find("ButtonStart").Enabled = True
    win.Find("TimelineButton").Enabled = True


def show_error(error_msg):
    items['status'].SetStyleSheet(BAD_CSS)
    items['status'].Text = f"ERROR:  {error_msg}"
    time.sleep(1)  # let the error sink in
    items['status'].SetStyleSheet(FANCY_CSS)
    # fusion.ShowConsole(True)
    print(error_msg)


# API documented at https://platform.openai.com/docs/guides/vision
# (This only uses standard Python libraries)
def api_query(base64_image, prompt, image_type):
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer " + settings["api_key"],
    }
    payload = {
        "model": settings["vision_model"],
        "messages": [
            {
                "role": "user",
                "content": [
                    {
                        "type": "text",
                        "text": prompt
                    },
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": f"data:image/{image_type};base64,{base64_image}"
                        }
                    }
                ]
            }
        ],
        "max_tokens": settings["max_tokens"]
    }

    parsed = urllib.parse.urlparse(settings["url"])
    if settings["allow_http"] and parsed.scheme == "http":
        connection = http.client.HTTPConnection(parsed.hostname, parsed.port)
    else:
        connection = http.client.HTTPSConnection(parsed.hostname, parsed.port)
    body_data = json.dumps(payload)
    connection.request("POST", "/v1/chat/completions", body=body_data, headers=headers)
    response = connection.getresponse()
    if response.status == 200:
        response_data = response.read().decode("utf-8")
        data = json.loads(response_data)
        return sanitize(data["choices"][0]["message"]["content"])
    else:
        connection.close()
        show_error(str(response.status) + ' ' + response.reason)
        raise Exception("Initial request failed with status: ", str(response.status) + ' ' + response.reason)


# This will need some real thought.  What types of query responses are to be considered invalid to be
# passed to the Resolve API?  For now maybe this will strip out some especially weird stuff, but I have no idea.
# It's probably useless actually... shrug
def sanitize(response):
    return unicodedata.normalize("NFD", response).encode("utf-8", "ignore").decode("utf-8").strip()


# create a window
def show_window(clips):
    global items

    def recheck_bin():
        nonlocal clips
        clips = collect_stills()
        if len(clips) > 0:
            items["ButtonStart"].SetText('Update metadata for ' + str(len(clips)) + ' image' + ("" if len(clips) == 1
                                                                                                else "s") +
                                         f" in bin (\"{folder.GetName()}\")")
            items["status"].SetText("Thanks! " + str(len(clips)) + ' image' + ("" if len(clips) == 1 else "s") +
                                    f" now found in bin (\"{folder.GetName()}\")")

    def close_window(ui):
        save_settings(ui)
        disp.ExitLoop()
        exit()

    def start_bin(ui):
        if len(clips) == 0:
            recheck_bin()
            return
        process_clips(clips, settings["describe_prompt"], settings["keyword_prompt"], False)

    def start_timeline(ui):
        process_clips(clips, settings["describe_prompt"], settings["keyword_prompt"], True)

    main_layout = ui.VGroup(
        [
            ui.Label(
                {'ID': 'instructions', 'Text': INSTRUCTION_TEXT,
                 'WordWrap': True, "Font": ui.Font({'PointSize': 14}), 'Weight': 0.0,
                 'Margin': 12, 'MinimumSize': [1200, 50], 'StyleSheet': FANCY_CSS,
                 'Alignment': {'AlignCenter': True}, }),
            ui.VGap(12),
            ui.HGroup(
                [ui.Label({'ID': 'URLLabel', 'Text': 'OpenAI API-compatible end-point', 'Weight': 0.0}),
                 ui.LineEdit({'ID': 'UrlLineEdit', 'Text': settings["url"], 'PlaceholderText': "https://host:port",
                              'Weight': 1.0})],
            ),
            ui.HGroup(
                [ui.Label({'ID': 'apiLabel', 'Text': 'API-Key (optional)', 'Weight': 0.0}),
                 ui.LineEdit(
                     {'ID': 'APIKeyLineEdit', 'Text': settings["api_key"], 'PlaceholderText': "Enter API key here",
                      'Weight': 1.0, 'EchoMode': "PasswordEchoOnEdit"})],
            ),
            ui.HGroup(
                [ui.Label({'ID': 'modelLabel', 'Text': 'Vision Model', 'Weight': 0.0}),
                 ui.LineEdit(
                     {'ID': 'ModelLineEdit', 'Text': settings["vision_model"],
                      'PlaceholderText': settings["vision_model"], 'Weight': 1.0, })],
            ),
            ui.HGroup(
                [ui.Label({'ID': 'maxTokensLabel', 'Text': 'Maximum tokens', 'Weight': 0.0}),
                 ui.SpinBox({'ID': 'MaxTokenSpinBox', 'Minimum': 200,
                             'Maximum': 2000, 'Weight': 1.0, })],
            ),
            ui.VGap(8),
            ui.HGroup(
                [ui.Label({'ID': 'DescribePromptLabel', 'Text': 'Description Prompt', 'Weight': 0.0}),
                 ui.LineEdit({'ID': 'DescribePromptText', 'Text': settings["describe_prompt"],
                              'PlaceholderText': settings["describe_prompt"],
                              'Weight': 1.0})],
            ),
            ui.HGroup(
                [ui.Label({'ID': 'KeywordPromptLabel', 'Text': 'Keyword Prompt', 'Weight': 0.0}),
                 ui.LineEdit(
                     {'ID': 'KeywordPromptText', 'Text': settings["keyword_prompt"],
                      'PlaceholderText': settings["keyword_prompt"],
                      'Weight': 1.0})],
            ),
            ui.HGroup(
                [ui.CheckBox({'ID': 'cbKeywords', 'Text': 'Generate Keywords', 'Weight': 0.0,
                              'Checked': settings["do_keywords"]}),
                 ui.CheckBox({'ID': 'cbDescription', 'Text': 'Generate Description', 'Weight': 0.0,
                              'Checked': settings["do_description"]}),
                 ui.CheckBox({'ID': 'cbDisableSSLVerification', 'Text':
                     'Disable SSL certificate verification for HTTPS (insecure)', 'Weight': 0.0,
                              'Checked': settings["do_disable_SSL_verification"]}),
                 ui.CheckBox({'ID': 'cbAllowHttp', 'Text':
                     'Use http without SSL (insecure)', 'Weight': 0.0,
                              'Checked': settings["allow_http"]}),
                 ],
            ),
            ui.VGap(8),
            ui.Tree({'ID': 'MetadataTree', 'ColumnCount': 3, "WordWrap": True, 'ExpandsOnDoubleClick': True,
                     "IconSize": 12, "VerticalScrollMode": True, "AutoScroll": True, 'MinimumSize': [500, 200],
                     'AlternatingRowColors': True, "Animated": True}),
            ui.VGap(8),
            ui.Label(
                {'ID': 'status', 'Text': str(len(clips)) + ' image' + ("" if len(clips) == 1 else "s") +
                                         f" found in bin (\"{folder.GetName()}\")",
                 'Alignment': {'AlignCenter': True},
                 'WordWrap': True, "Font": ui.Font({'PointSize': 12}), 'Weight': 1.0, 'MinimumSize': [100, 100],
                 'Margin': 12, 'StyleSheet': FANCY_CSS}, ),
            ui.HGroup([
                ui.Label(
                    {'ID': 'binLabel', 'Text': "Bin Mode\n(Use In Media Page)",
                     'WordWrap': True, "Font": ui.Font({'PointSize': 18}), 'Weight': 1.0,
                     'Margin': 12, 'StyleSheet': FANCY_CSS,
                     'Alignment': {'AlignCenter': True}, }),
                ui.Label(
                    {'ID': 'timelineLabel', 'Text': "Timeline Mode\n(Use In Color Page)",
                     'WordWrap': True, "Font": ui.Font({'PointSize': 18}), 'Weight': 1.0,
                     'Margin': 12, 'StyleSheet': FANCY_CSS,
                     'Alignment': {'AlignCenter': True}, }),
            ]),
            ui.HGroup([
                ui.Button(
                    {'ID': 'ButtonStart',
                     'Text': 'Update metadata for ' + str(len(clips)) + ' image' + ("" if len(clips) == 1
                                                                                    else "s") +
                             f" in \"{folder.GetName()}\" bin",
                     "Font": ui.Font({'PointSize': 12}), "Default": True, 'Weight': 1, 'Margin': 12, }),
                ui.Button(
                    {'ID': 'TimelineButton',
                     'Text': 'Update clip selected on timeline',
                     "Font": ui.Font({'PointSize': 12}), "Default": True, 'Weight': 1, 'Margin': 12, }),
            ]),
            ui.Label(
                {'ID': 'note', 'Text': NOTE_TEXT,
                 'WordWrap': True, "Font": ui.Font({'PointSize': 10}), 'Weight': 1.0,
                 'Margin': 12, 'MinimumSize': [100, 120], }),
        ],
    )

    win.AddChild(main_layout)

    items = win.GetItems()
    items["MaxTokenSpinBox"].Value = settings["max_tokens"]  # not set above for some reason so do manually
    items["MetadataTree"].SetHeaderLabels(["Item", "Type", "Metadata"])
    win.On.TagWin.Close = close_window
    win.On['UrlLineEdit'].TextChanged = update_settings
    win.On['APIKeyLineEdit'].TextChanged = update_settings
    win.On['ModelLineEdit'].TextChanged = update_settings
    win.On['MaxTokenSpinBox'].ValueChanged = update_settings
    win.On['DescribePromptText'].TextChanged = update_settings
    win.On['KeywordPromptText'].TextChanged = update_settings
    win.On['cbKeywords'].Clicked = update_settings
    win.On['cbDescription'].Clicked = update_settings
    win.On['cbDisableSSLVerification'].Clicked = update_settings
    win.On['cbAllowHttp'].Clicked = update_settings
    if len(clips) == 0:
        show_error("No image Clips found in current bin.  Select a bin containing at least one still image.")
        items["ButtonStart"].SetText("Recheck Bin")
    win.On.ButtonStart.Clicked = start_bin
    items["TimelineButton"].SetToolTip(
        "Select a clip on a timeline and a representative frame")
    items["ButtonStart"].SetToolTip("Select a bin in the media pool that contains one more more still images")
    win.On.TimelineButton.Clicked = start_timeline
    win.Show()
    disp.RunLoop()
    win.Hide()


# main
load_settings()
filtered = collect_stills()
show_window(filtered)
